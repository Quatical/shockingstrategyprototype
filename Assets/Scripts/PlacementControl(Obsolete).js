﻿#pragma strict
var buildObject : GameObject;
var buildCooldown = 1.0;
var style : GUIStyle;

private var guideInstance : GameObject;
private var canBuild : boolean;

private var cooldown : boolean;
private var countdown : float;

function Start () {
	canBuild = true;
	cooldown = true;
	countdown = 0.0;
}

function OnGUI (){
	var cooldownGUI = countdown;
	if (cooldownGUI < 0) {
		cooldownGUI = 0;
	}
	
	GUI.Label(Rect(10, 30, 100, 20), ("Tower Cooldown: " + cooldownGUI), style);
}

function Update () {
	if (canBuild == true) 
	{
		if (Input.GetKeyDown (KeyCode.Space))
		{
			Instantiate(buildObject);
		}
	}
	
	if (Input.GetKeyDown(KeyCode.Escape))
	{
		Application.Quit();
	}
		
	if (Input.GetKeyDown(KeyCode.R))
	{
		 Application.LoadLevel(0);
	}
	
	countdown -= Time.deltaTime;
}

function setCanBuild (state : boolean) {
	canBuild = state;
}

function resetCooldown () {
	cooldown = false;
	countdown = buildCooldown;
}

function getCooldown(target : Component){
	if (countdown <= 0.0)
	{
		cooldown = true;
	} else
	{
		cooldown = false;
	}
	target.SendMessage("setCooldown", cooldown);
}
