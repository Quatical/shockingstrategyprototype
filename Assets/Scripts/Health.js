﻿#pragma strict
var maxHP : int;
var currentHP : int;

var HealthBarPrefab : GameObject;
private var HPBar : Transform;

function Start () 
{
	currentHP = maxHP;
	var MyPos : Vector3 = transform.position;
	MyPos.y += 3;
	var HPCanvus : GameObject = Transform.Instantiate(HealthBarPrefab, MyPos, transform.rotation);
	HPCanvus.transform.SetParent(transform);
	HPBar = HPCanvus.transform.GetChild(1);
	
}

function Update () 
{
	if (currentHP <= 0)
	{
		GameObject.Destroy(this.gameObject);
	}
	
	HealthBarUpdate();
}

function HealthBarUpdate () {
	HPBar.transform.localScale.x = currentHP * (0.9 / maxHP);
}

function Hit (damage : int) 
{
	currentHP -= damage;
}

function getHP () : int
{
	return currentHP;
}

function resetHP () {
	currentHP = maxHP;
}

function getMaxHP () : int
{
	return maxHP;
}