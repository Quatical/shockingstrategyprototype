﻿#pragma strict

var GameUI : GameObject;
var GameOver : GameObject;
var StartPoint : GameObject;

var ScoreboardText : UI.Text;
var MaxDistance : float = 0;
var sheeps : Array = new Array();

function Start () {
	GameUI.SetActive(true);
}

function Reload () {
	Application.LoadLevel(0);
}

function setDistance (sheep : GameObject) {
	sheeps.Add(sheep);
}

function LateUpdate () {
	var currDist : float = 0;
	var newMax : float = 0;
	for (var sheep : GameObject in sheeps) {
		if (sheep == null) {
			sheeps.remove(null);
		} else {
			currDist = Vector3.Distance(StartPoint.transform.position, sheep.transform.position);
			if (currDist >= newMax) {
				newMax = currDist;
			}
		}
	}
	
	if (sheeps.length == 0) {
		GameOver.SetActive(true);
	} else {
		MaxDistance = Mathf.RoundToInt(newMax);
		ScoreboardText.text = "Distance: " + MaxDistance;
	}
}