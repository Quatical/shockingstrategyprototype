﻿#pragma strict

var attackRange : int = 10;
var attackDamage : int = 10;
var attackCooldown : float = 2.0;
var OpponentTeam : String;
var StartCommand : GameObject;

private var attackCD = 0.0;
private var startCMD = false;

function Start () {
	attackCD = Time.time;
	//StartCommand = GameObject.Find("StateControl");
}

function FixedUpdate () {
	if (startCMD) {
		var enemy : GameObject;
		enemy = findNearestEnemy();
		if (enemy != null)
		{
			if (Vector3.Distance(enemy.transform.position, transform.position) <= attackRange && Time.time > attackCD)
			{
				enemy.GetComponent("Health").SendMessage("Hit", attackDamage);
				attackCD = Time.time + attackCooldown;
			}
		}
	} else
	{
		requestStartCMD();
	}
}

function findNearestEnemy() : GameObject
{
	var allEnemies : GameObject[];
	allEnemies = GameObject.FindGameObjectsWithTag(OpponentTeam + "_Team");
	var closestEnemy : GameObject;
	var MaxDistance = Mathf.Infinity;
	
	if (allEnemies.Length == 0)
	{
		return null;
	} else
	{
		for (var currEnemy : GameObject in allEnemies)
		{
			var currDistance = Vector3.Distance(currEnemy.transform.position, transform.position);
			if (currDistance < MaxDistance)
			{
				closestEnemy = currEnemy;
				MaxDistance = currDistance;
			}
		}
		return closestEnemy;
	}
}

function requestStartCMD () {
	//StartCommand.GetComponent("StateCTRL").SendMessage("requestStart", this.gameObject);
}

function responseStartCMD (response : boolean) {
	startCMD = response;
}

function setOpponentTeam (team : String){
	OpponentTeam = team;
	if (team == "Red") {
		transform.GetComponent.<Renderer>().material.color = Color.blue;
		transform.tag = "Blue_Team";
		this.gameObject.layer = 8;	
	} else {
		transform.GetComponent.<Renderer>().material.color = Color.red;
		transform.tag = "Red_Team";
		this.gameObject.layer = 9;	
	}
	responseStartCMD(true);
}