﻿#pragma strict

var DaySheepSpawner : Component;
var DayTime : boolean = true;
var Team : boolean = true;

var Food : float;
var Population : int;
//var Premium : int;

var Style : GUIStyle;
var BasePrefab : GameObject;

var IncomeCountdown : float = 0.5f;
private var IncomeTimeLeft : float = 0.0f;

var FeedCountdown : float = 4.0f;
private var FeedTimeLeft : float = 0.0f;

var FoodIncome : float = 0.5f;
var PopulationGrowth : int = 2;

var FloorColor : Component;

function DisplayResources () {
	GUI.Label(Rect ((30), (Screen.height - 30), 100, 20), ("Food: " + Mathf.RoundToInt(Food)), Style);
	GUI.Label(Rect ((250), (Screen.height - 30), 100, 20), ("Population: " + Population), Style);
	
	if (DayTime) {
		GUI.Label(Rect ((180), (Screen.height - 130), 100, 20), ("Add Population"), Style);
		
		if (GUI.Button(Rect((50), (Screen.height - 100), 60, 50), "- -")) {
			subtractPopulationGrowth(10);
		}
	
		if (GUI.Button(Rect((110), (Screen.height - 100), 50, 50), "-")) {
			subtractPopulationGrowth(1);
		}
		
		if (GUI.Button(Rect((160), (Screen.height - 100), 150, 50), ("Breed: " + PopulationGrowth))) {
			GrowPopulation();
		}
		
		if (GUI.Button(Rect((310), (Screen.height - 100), 50, 50), "+")) {
			addPopulationGrowth(1);
		}
		
		if (GUI.Button(Rect((360), (Screen.height - 100), 60, 50), "+ +")) {
			addPopulationGrowth(10);
		}
	}
}

function addPopulationGrowth (amount : int) {
	PopulationGrowth += amount;
}

function subtractPopulationGrowth (amount : int) {
	PopulationGrowth -= amount;
	if (PopulationGrowth < 0) {
		PopulationGrowth = 0;
	}
}

function FeedPopulation () {
	var newFood : float = Food - Population;
	if (newFood < 0) {
		var subtract = Population - Mathf.RoundToInt(Food);
		Population -= subtract;
		for (var i=1; i<subtract;i++) {
			DaySheepSpawner.SendMessage("SetTeam", Team);
			DaySheepSpawner.SendMessage("RemoveSheep");
			print(i);
		}		
		Food = 0;
	} else {
		Food = newFood;
	}
}

function GrowPopulation () {
	var newFood = Food - PopulationGrowth;
	if (newFood >= 0) {
		Population += PopulationGrowth;
		Food = newFood;
	} else {
		PopulationGrowth += newFood;
		Population += PopulationGrowth;
		Food = 0;
	}
	
	for (var i=1;i<PopulationGrowth;i++) {
		DaySheepSpawner.SendMessage("SetTeam", Team);
		DaySheepSpawner.SendMessage("AddSheep");
	}
}

function GrowFood () {
	var FoodMultiplier : float;
	FoodMultiplier = Food / 100;
	Food += FoodIncome + FoodMultiplier;
	
	if (Food > 1000) {
		Food = 1000;
	}		
}

function setDay (state : boolean) {
	DayTime = state;
	DaySheepSpawner.SendMessage("SetTeam", Team);
}

function removePopulation() {
	Population -= 1;
}

function addFood(amount : float) {
	print(amount);
	Food += amount;
}

function subFood(amount : float) {
	Food -= amount;
}


function addPopulation() {
	Population += 1;
}

function getPopulation(sender : Component) {
	sender.SendMessage("setPopulation", Population);
}

function getFood(sender : Component) {
	sender.SendMessage("setFood", Food);
}

function OnGUI () {
	if (DayTime) {
		DisplayResources();
	}
}

function Start () {
	FeedTimeLeft = FeedCountdown;
	IncomeTimeLeft = IncomeCountdown;
	
	if (Team) {
		FloorColor = GameObject.Find("Blue_Floor").GetComponent("GrassColor");
	} else {
		FloorColor = GameObject.Find("Red_Floor").GetComponent("GrassColor");
	}
	
	DaySheepSpawner = GameObject.Find("StateMachine").GetComponent("DaySheepSpawner");
}

function Update () {
	if (DayTime) {
		/*if (Team) {
			if (GameObject.Find("Blue_Base(Clone)") == null) {
				GameObject.Instantiate(BasePrefab, transform.position, transform.rotation);
			} else {
				GameObject.Find("Blue_Base(Clone)").GetComponent("Health").SendMessage("resetHP");
			}	
		} else {
			if (GameObject.Find("Red_Base(Clone)") == null) {
				GameObject.Instantiate(BasePrefab, transform.position, transform.rotation);
			} else {
				GameObject.Find("Red_Base(Clone)").GetComponent("Health").SendMessage("resetHP");
			}
		}*/
		
		if (IncomeTimeLeft <= 0.0f) {
			GrowFood();
			IncomeTimeLeft = IncomeCountdown;
		} else {
			IncomeTimeLeft -= Time.deltaTime;
		}
		
		if (FeedTimeLeft <= 0.0f) {
			FeedPopulation();
			FeedTimeLeft = FeedCountdown;
		} else {
			FeedTimeLeft -= Time.deltaTime;
		}
	}
	FloorColor.SendMessage("SetFood", Food);
}