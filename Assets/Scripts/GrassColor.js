﻿#pragma strict

var Food : float = 0;

function SetFood (FoodValue : float) {
	Food = FoodValue;
}

function GrassColorFloat() : float {
	var FoodColor : float = Food / 1000;
	if (FoodColor > 1.0) {
		FoodColor = 1.0;
	}
	
	if (FoodColor < 0) {
		FoodColor = 0;
	}
	
	return FoodColor;
}

function GrassColor (f : float) : Color {
	var grassColor : Color = Vector4((1.0 - (f / 1.1)), 1.0, (1.0 - f));
	return grassColor;
}


function Start () {

}

function Update () {
	transform.GetComponent.<Renderer>().material.color = GrassColor(GrassColorFloat());
}