﻿#pragma strict

var Team : boolean = false;
private var OpponentTeam : String = "";

var MaxMoveSpeed : float = 10;
var SlowMoveSpeed : float = 5;
var WonderingMoveSpeed : float = 2;
private var CurrentMoveSpeed : float = 0;

var Slowed : boolean = false;
var SlowCooldown : float = 5;
private var SlowCooldownCT : float = 0;

var WonderingRange : float = 20;
var WonderingTimeRange : float = 3;
private var WonderingTimeCD : float = 0;

var Following : boolean = false;
var Wondering : boolean = false;
private var MyLookAt : GameObject;
var TargetPos : GameObject;

private var MouseOver : boolean = false;
private var BlueResource : Component;
private var RedResource : Component;
private var Food : float;
private var EatCD : float = 0;

private var OldPos : Vector3;
var EatMultiplier : float = 1;

function SetTeam(team : boolean) {
	Team = team;
	
	if (Team) {
		transform.tag = "Blue_Team";
		transform.GetComponent.<Renderer>().material.color = Color.blue;
		this.gameObject.layer = 8;
		OpponentTeam = "Red";
		transform.name = "DaySheepBlue";
	} else {
		transform.tag = "Red_Team";
		transform.GetComponent.<Renderer>().material.color = Color.red;
		this.gameObject.layer = 9;
		OpponentTeam = "Blue";
		transform.name = "DaySheepRed";
	}
}

function ActivateSlow () {
	SlowCooldownCT = SlowCooldown;
	Slowed = true;
}

function SelectRandomLocation () : Vector3 {
	return Vector3(Random.Range((transform.position.x - WonderingRange), (transform.position.x + WonderingRange)), 0.25, Random.Range((transform.position.z - WonderingRange), (transform.position.z + WonderingRange)));
}

function MoveTowards(target : GameObject)
{
	if ((Vector3.Distance(target.transform.position, transform.position) > 0.1)) 
	{
		transform.LookAt(target.transform);
		transform.position += transform.forward * CurrentMoveSpeed * Time.deltaTime;
	}
}

function Die () {
	if (Team) {
		//BlueResource.SendMessage("addPopulation");
	} else {
		//RedResource.SendMessage("addPopulation");
	}
	print("Died");
	GameObject.Destroy(MyLookAt.gameObject);
	GameObject.DestroyImmediate(this.gameObject);
}

function setFood (amount : float) {
	Food = amount;
}

function Eat() {
	if (EatCD <= 0) {
		var amount : float = 0;
		var NewPos = transform.position;
		amount = Vector3.Distance(NewPos, OldPos) * EatMultiplier;
		//print(amount);
				
		if (Team) {
			RedResource.SendMessage("getFood", this);
			if (Food > 0) {
				BlueResource.SendMessage("addFood", amount);
				RedResource.SendMessage("subFood", amount);
			}
		} else {
			BlueResource.SendMessage("getFood", this);
			if (Food > 0) {
				RedResource.SendMessage("addFood", amount);
				BlueResource.SendMessage("subFood", amount);
			}
		}	
		OldPos = NewPos;
		EatCD = 1.0;
	} else {
		EatCD -= Time.deltaTime;
	}
}

function Start () {
	BlueResource = GameObject.Find("ResourceHolderBlue").GetComponent("Resources");
	RedResource = GameObject.Find("ResourceHolderRed").GetComponent("Resources");
	
	CurrentMoveSpeed = MaxMoveSpeed;
	MyLookAt = new GameObject("SleepLookAt");
	TargetPos = MyLookAt;
	TargetPos.transform.position = transform.position;
	OldPos = transform.position;
}

function Update () {
	// Slow Time Effect
	if (Slowed) {
		if (SlowCooldownCT <= 0) {
			CurrentMoveSpeed = MaxMoveSpeed;
			Slowed = false;
		} else {
			CurrentMoveSpeed = SlowMoveSpeed;
			SlowCooldownCT -= Time.deltaTime;
		}
	}
	
	if (Wondering) {
		CurrentMoveSpeed = WonderingMoveSpeed;
		if (WonderingTimeCD <= 0) {
			var choice : int = Random.Range(0, 2);
			//print(choice);
			if (choice == 0) {
				TargetPos.transform.position = transform.position;
				WonderingTimeCD = WonderingTimeRange;
			} else {
				TargetPos.transform.position = SelectRandomLocation();
				//print("Wondering to: " + TargetPos.transform.position);
				WonderingTimeCD = WonderingTimeRange;
			}
		} else {
			WonderingTimeCD -= Time.deltaTime;
			MoveTowards(TargetPos);
		}
	}
	
	if (Following) {
		MoveTowards(TargetPos);
	}
	
	if (MouseOver) {
		if (Input.GetMouseButtonDown(0)) {
			if (Following) {
				Following = false;
				
				TargetPos = MyLookAt;
				
				Wondering = true;
			} else {
				Wondering = false;
				TargetPos = GameObject.Find("SheepFollowGuide");
				Following = true;
				
				if (!Slowed) {
					CurrentMoveSpeed = MaxMoveSpeed;
				} else {
					CurrentMoveSpeed = SlowMoveSpeed;
				}
			}
		}
	}
	
	//Eat();
}

function OnMouseEnter () {
	transform.GetComponent.<Renderer>().material.color = Color.cyan;
	MouseOver = true;
}

function OnMouseExit () {
	if (Team) {
		transform.GetComponent.<Renderer>().material.color = Color.blue;
	} else {
		transform.GetComponent.<Renderer>().material.color = Color.red;
	}	
	MouseOver = false;
}