﻿#pragma strict
var maxHunger : float = 100;
var currentHunger : float;

var BarPrefab : GameObject;
var Owner : GameObject;
private var Bar : Transform;
private var Canvus : GameObject;

function Start () 
{
	currentHunger = maxHunger;
	var MyPos : Vector3 = transform.position;
	MyPos.y += 3;
	Canvus = Transform.Instantiate(BarPrefab, MyPos, transform.rotation);
	Canvus.transform.SetParent(transform);
	Bar = Canvus.transform.GetChild(1);
	
}

function SetHunger (k : float) {
	currentHunger = k;
}

function GetHunger () : float {
	return currentHunger;
}

function ResetHunger() {
	currentHunger = maxHunger;
}

function Update () 
{	
	Bar.transform.localScale.x = currentHunger * (0.9 / maxHunger);
}