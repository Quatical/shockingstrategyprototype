﻿#pragma strict

var DaySheepSpawner : Component;
var Dragon : GameObject;
var Golem : GameObject;
//var Minion : GameObject;

var skin : GUIStyle;

// True is blue, False is red
var Team : boolean;

private var Population : int;
private var BlueResourceHolder : Component;
private var RedResourceHolder : Component;

private var Blocked : boolean = false;
private var obstructionBlocked : boolean = false;
private var buildMode : boolean = false;
private var Selected : int = 0;
private var obstruction : GameObject = null;
private var held : GameObject = null;
private var holdingObject : boolean = false;
private var obstructionYPos : float = 0;

function Start() {
	transform.GetComponent.<Renderer>().material.color = Color.green;
	Blocked = false;
	
	BlueResourceHolder = GameObject.Find("ResourceHolderBlue").GetComponent("Resources");
	RedResourceHolder = GameObject.Find("ResourceHolderRed").GetComponent("Resources");
	DaySheepSpawner = GameObject.Find("StateMachine").GetComponent("DaySheepSpawner");
}

function Update() {
	transform.rotation.x = 0;
	transform.rotation.y = 0;
	transform.rotation.z = 0;
	transform.position.y = 0.2;
	PositionRaycast();
	if (!holdingObject) {
		if (Input.GetKeyDown(KeyCode.B)) {
			buildMode = !buildMode;
		}
	}
	
	if (buildMode) {
		buildUpdate();
	} else { 
		moveUpdate();
	}	
}

function OnGUI() {
	if (buildMode) {
		var SelectedName : String;
		if (Selected == 0) {
			SelectedName = "Dragon";
		} else {
			if (Selected == 1) {
				SelectedName = "Golem";
			}/* else {
				SelectedName = "Minion";
			}*/
		}
		GUI.Label(Rect ((Screen.width - 290), (Screen.height - 70), 160, 20), ("Arrow keys change"), skin);
		GUI.Label(Rect ((Screen.width - 290), (Screen.height - 40), 160, 20), ("Currently Selected: " + SelectedName), skin);
	} else {
		GUI.Label(Rect ((Screen.width - 270), (Screen.height - 30), 160, 20), ("Press B for buildmode"), skin);
	}
}

function OnTriggerEnter (other : Collider)
{	
	if (buildMode) {
		transform.GetComponent.<Renderer>().material.color = Color.red;
		Blocked = true;
	} else {
		if (!holdingObject) {
			transform.GetComponent.<Renderer>().material.color = Color.yellow;
			obstruction = other.gameObject;
			obstructionBlocked = true;
		} else {
			transform.GetComponent.<Renderer>().material.color = Color.red;
			obstructionBlocked = true;
		}
	}
}

function OnTriggerExit (other : Collider)
{
	transform.GetComponent.<Renderer>().material.color = Color.green;
	Blocked = false;
	obstructionBlocked = false;	
	obstruction = null;
}

function setPopulation(amount : int) {
	Population = amount;
}

function getPopulation() {
	if (Team) {
		BlueResourceHolder.SendMessage("getPopulation", this);
	} else {
		RedResourceHolder.SendMessage("getPopulation", this);
	}
}

function removePopulation() {
	if (Team) {
		BlueResourceHolder.SendMessage("removePopulation");
	} else {
		RedResourceHolder.SendMessage("removePopulation");
	}
	DaySheepSpawner.SendMessage("RemoveSheep");
}

function PositionRaycast() {
	var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
	var hit : RaycastHit;
	var layermask : int;
	var newPos : Vector3;
	if (Team) {
		layermask = 1 << 10;
		if (Physics.Raycast (ray, hit, 100, layermask)) 
		{
			newPos = hit.point;
			transform.localPosition.x = (Mathf.Round(newPos.x));
			transform.localPosition.z = (Mathf.Round(newPos.z));
		}
	} else {
		layermask = 1 << 11;
		if (Physics.Raycast (ray, hit, 100, layermask)) 
		{
			newPos = hit.point;
			transform.localPosition.x = (Mathf.Round(newPos.x));
			transform.localPosition.z = (Mathf.Round(newPos.z));
		}
	}
}

function buildUpdate() {
	if (Input.GetKeyDown(KeyCode.RightArrow)) {
		Selected += 1;
		if (Selected > 1) {
			Selected = 0;
		}
	}
	
	if (Input.GetKeyDown(KeyCode.LeftArrow)) {
		Selected -= 1;
		if (Selected < 0) {
			Selected = 1;
		}
	}
	
	if (Input.GetKeyDown(KeyCode.Space)) {
		if (!Blocked) {
			var instObject : GameObject;
			var TeamName : String;
			// Inverted for enemy team see other script
			if (Team) {
				TeamName = "Red";
			} else {
				TeamName = "Blue";
			}
			
			getPopulation();
			
			if (Population >= 1) {
					if (Selected == 0) {
						instObject = GameObject.Instantiate(Dragon, transform.position, transform.rotation);
						instObject.GetComponent("DragonMarker").SendMessage("SetTeam", Team);
					} else {
						if (Selected == 1) {
							instObject = GameObject.Instantiate(Golem, transform.position, transform.rotation);
							instObject.GetComponent("Golem").SendMessage("SetTeam", Team);
						}
					}
				removePopulation();
			}
		}
	}
}

function moveUpdate() {
	if (Input.GetKeyDown(KeyCode.Space)) {
		if (holdingObject) {
			if (!obstructionBlocked) {
				held.transform.parent = null;
				held.transform.localPosition.y = obstructionYPos;
				held.GetComponent.<Collider>().enabled = true;
				held = null;
				holdingObject = false;
			}
		} else {
			if (obstruction != null) {
				held = obstruction;
				obstructionYPos = held.transform.localPosition.y;
				held.transform.parent = this.transform;
				held.GetComponent.<Collider>().enabled = false;	
				holdingObject = true;
				
				obstruction = null;
				obstructionBlocked = false;	
				transform.GetComponent.<Renderer>().material.color = Color.green;
			}
		}
	}
}

function setTeam(team : boolean) {
	Team = team;
}