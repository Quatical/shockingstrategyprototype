﻿#pragma strict

var Target : GameObject;
var CanAttack : boolean = false;

var MinimumRange : float = 1.0;
var MoveSpeed : float = 30;

private var EndPos : GameObject;


function SetTarget (target : GameObject) {
	Target = target;
}

function Attack () {
	CanAttack = true;
}

function MoveTowards()
{
	if ((Vector3.Distance(Target.transform.position, transform.position) > MinimumRange)) 
	{
		transform.LookAt(Target.transform);
		transform.position += transform.forward * MoveSpeed * Time.deltaTime;
	}
}

function MoveAway () {
	if ((Vector3.Distance(EndPos.transform.position, transform.position) > MinimumRange)) 
	{
		transform.LookAt(EndPos.transform);
		transform.position += transform.forward * MoveSpeed * Time.deltaTime;
	}
}

function Start () {
	EndPos = new GameObject("DragonEndingLocation");
	EndPos.transform.position = Vector3(0, 1000, 0);
}

function Update () {
	if (CanAttack) {
		if (Target != null) {
			var CurrDistance = Vector3.Distance(Target.transform.position, transform.position);
			if (CurrDistance <= MinimumRange) {
				Target.gameObject.GetComponent("Sheep").SendMessage("Die");
			} else { 
				MoveTowards();
			}
		} else {
			MoveAway();
			
			if ((Vector3.Distance(EndPos.transform.position, transform.position) <= MinimumRange)) 
			{	
				GameObject.Destroy(EndPos.gameObject);
				GameObject.Destroy(this.gameObject);
			}
		}
	}
}