﻿#pragma strict

var DaySheepSpawner : Component;

var buildingGuide : GameObject;
var Sheep : GameObject;
var Team : boolean = false;
var DayNightTime : boolean = true;

private var canAttack : boolean = false;
private var newPos : Vector3;

private var BlueResourceHolder : Component;
private var RedResourceHolder : Component;
private var stateMachine : Component;
private var Population : int;
private var Food : float;

function Start () {
	BlueResourceHolder = GameObject.Find("ResourceHolderBlue").GetComponent("Resources");
	RedResourceHolder = GameObject.Find("ResourceHolderRed").GetComponent("Resources");
	stateMachine = GameObject.Find("StateMachine").GetComponent("StateMachine");
	DaySheepSpawner = GameObject.Find("StateMachine").GetComponent("DaySheepSpawner");
}

function Update () {

	if (canAttack) {
			/*if (Team) {
				if (GameObject.Find("Red_Base(Clone)") == null) {
					BlueResourceHolder.SendMessage("getFood", this);
					print(Food);
					Food = Food / 10;
					print(Food);
					BlueResourceHolder.SendMessage("addFood", Food);
					canAttack = false;
					removeSheep();
					stateMachine.SendMessage("done");
					
				}
		} else {
			if (GameObject.Find("Blue_Base(Clone)") == null) {
				RedResourceHolder.SendMessage("getFood", this);
				print(Food);
				Food = Food / 10;
				print(Food);
				RedResourceHolder.SendMessage("addFood", Food);
				canAttack = false;
				removeSheep();
				stateMachine.SendMessage("done");
			}
		}*/
						
		if (Input.GetKeyDown(KeyCode.Space)) {
			getPopulation();
			if (Population >= 1) {
				PositionRaycast();
				var instObject : GameObject = GameObject.Instantiate(Sheep, newPos, buildingGuide.transform.rotation);
				if (Team) {
					instObject.GetComponent("Sheep").SendMessage("SetTeam", true);
					BlueResourceHolder.SendMessage("removePopulation");
					DaySheepSpawner.SendMessage("SetTeam", true);
					DaySheepSpawner.SendMessage("RemoveSheep");
				} else {
					instObject.GetComponent("Sheep").SendMessage("SetTeam", false);
					RedResourceHolder.SendMessage("removePopulation");
					DaySheepSpawner.SendMessage("SetTeam", false);
					DaySheepSpawner.SendMessage("RemoveSheep");
				}
			}
		}
	}
}

function FlipTime (day : boolean) {
	// Both Team
	DayNightTime = day;
	if (DayNightTime) {
		// Daytime
		buildingGuide.SetActive(true);
		buildingGuide.GetComponent("BlockPlacementCTRL").SendMessage("setTeam", Team);
		canAttack = false;
	} else {
		// Nighttime
		buildingGuide.SetActive(false);
		canAttack = true;
	}	
}

// Minion Placement stuff
function PositionRaycast() {
	var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
	var hit : RaycastHit;
	var layermask : int;
	
	if (!Team) {
		layermask = 1 << 10;
	} else {
		layermask = 1 << 11;
	}
	
	if (Physics.Raycast (ray, hit, 100, layermask)) 
	{
		newPos = hit.point;
		newPos.y += 1;
	}
}

function SetTeam (i : boolean) {
	Team = i;
}

function setFood(amount : float) {
	Food = amount;
}

function setPopulation(amount : int) {
	Population = amount;
}

function getFood() {
	if (Team) {
		BlueResourceHolder.SendMessage("getFood", this);
	} else {
		RedResourceHolder.SendMessage("getFood", this);
	}
}

function getPopulation() {
	if (Team) {
		BlueResourceHolder.SendMessage("getPopulation", this);
	} else {
		RedResourceHolder.SendMessage("getPopulation", this);
	}
}

function removeSheep() {
	var minions : GameObject[];
	
	if (Team) {
		minions = GameObject.FindGameObjectsWithTag("Blue_Team");
		DaySheepSpawner.SendMessage("SetTeam", true);
	} else {
		minions = GameObject.FindGameObjectsWithTag("Red_Team");
		DaySheepSpawner.SendMessage("SetTeam", false);
	}
	
	for (var minion in minions) {
		if (minion.name == "Sheep(Clone)") {
			minion.GetComponent("Sheep").SendMessage("Die");
			DaySheepSpawner.SendMessage("AddSheep");
		}
	}
}
