﻿#pragma strict

function OnTriggerEnter (other : Collider) {
	other.GetComponent("Sheep").SendMessage("Feed");
	GameObject.Destroy(this.gameObject);
}