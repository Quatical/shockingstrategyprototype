﻿#pragma strict

// Golems grab hold of nearby sheep slowing them down.

var Team : boolean;
var AttackCooldown : float = 2;
var AttackRange : float = 10;

private var AttackCooldownCD : float = 0;
private var OpponentTeam : String = "";

function SetTeam(team : boolean) {
	Team = team;
	
	if (Team) {
		transform.tag = "Blue_Team";
		transform.GetComponent.<Renderer>().material.color = Color.blue;
		this.gameObject.layer = 8;
		OpponentTeam = "Red";
	} else {
		transform.tag = "Red_Team";
		transform.GetComponent.<Renderer>().material.color = Color.red;
		this.gameObject.layer = 9;
		OpponentTeam = "Blue";
	}
}

function findNearestEnemy() : GameObject
{
	var allEnemies : GameObject[];
	allEnemies = GameObject.FindGameObjectsWithTag(OpponentTeam + "_Team");
	var closestEnemy : GameObject;
	var MaxDistance = Mathf.Infinity;
	
	if (allEnemies.Length == 0)
	{
		return null;
	} else
	{
		for (var currEnemy : GameObject in allEnemies)
		{
			var currDistance = Vector3.Distance(currEnemy.transform.position, transform.position);
			if (currDistance < MaxDistance)
			{
				closestEnemy = currEnemy;
				MaxDistance = currDistance;
			}
		}
		if (Vector3.Distance(closestEnemy.transform.position, transform.position) <= AttackRange) {
			return closestEnemy;
		} else {
			return null;
		}
	}
}

function activateSlow (target : GameObject) {
	if (AttackCooldownCD <= 0) {
		target.GetComponent("Sheep").SendMessage("ActivateSlow");
		AttackCooldownCD = AttackCooldown;
	}
}

function Start () {

}

function Update () {
	if (transform.tag != "Untagged") {
		var Target : GameObject = findNearestEnemy();
		
		if (Target != null) {
			if (Target.name == "Sheep(Clone)") {
				activateSlow(Target);
			}
		}
	}
	AttackCooldownCD -= Time.deltaTime;
}