﻿#pragma strict

// Detects Sheep and feeds them to the dragon

var Dragon : GameObject;

var SpawnCooldown : float = 2;
private var SpawnCooldownTL : float = 0;

var Target : GameObject;

function SpawnDragon() {
	if (Target != null) {
		var MyDragon : GameObject = GameObject.Instantiate(Dragon, Vector3(transform.position.x, transform.position.y + 100, transform.position.z), transform.rotation);
		var MyDragonComponent : Component = MyDragon.gameObject.GetComponent("Dragon");
		
		MyDragonComponent.SendMessage("SetTarget", Target);
		MyDragonComponent.SendMessage("Attack");
		
		Target.gameObject.GetComponent("Sheep").SendMessage("Trap");
		Target = null;
	}
}

function Start () {
	SpawnCooldownTL = SpawnCooldown;
}

function Update () {
	if (SpawnCooldownTL <= 0) {
		if (Target != null) {
			SpawnDragon();
		}
	} else {
		SpawnCooldownTL -= Time.deltaTime;
	}
}

function OnTriggerEnter (other : Collider) {
	Target = other.gameObject;
}