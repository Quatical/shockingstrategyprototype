﻿#pragma strict
var VerticalDampner = 50;
var HorizontalDampner = 75;
var MoveSpeed = 10;
var ZoomSpeed = 100;


function Start () {

}

function Update () {
	 if (Input.GetAxis("Mouse ScrollWheel") > 0) //forward
	 {
	 	Camera.main.transform.Translate(Vector3.down * ZoomSpeed * Time.deltaTime, Space.World);
	 	Camera.main.transform.Translate(Vector3.back * MoveSpeed * Time.deltaTime, Space.World);
	 	Camera.main.transform.Translate(Vector3.left * MoveSpeed * Time.deltaTime, Space.World);
	 }
	 
	 if (Input.GetAxis("Mouse ScrollWheel") < 0) //backward
	 {
	 	Camera.main.transform.Translate(Vector3.up * ZoomSpeed * Time.deltaTime, Space.World);
	 	Camera.main.transform.Translate(Vector3.forward * MoveSpeed * Time.deltaTime, Space.World);
	 	Camera.main.transform.Translate(Vector3.right * MoveSpeed * Time.deltaTime, Space.World);
	 }
	 
	 var mousePos = Input.mousePosition;
	 
	 if (mousePos.x >= (Screen.width - HorizontalDampner))
	 {
	 	Camera.main.transform.Translate(Vector3.left * MoveSpeed * Time.deltaTime, Space.World);
	 	Camera.main.transform.Translate(Vector3.forward * MoveSpeed * Time.deltaTime, Space.World);
	 }
	 
	 if (mousePos.x <= HorizontalDampner)
	 {
	 	Camera.main.transform.Translate(Vector3.right * MoveSpeed * Time.deltaTime, Space.World);
	 	Camera.main.transform.Translate(Vector3.back * MoveSpeed * Time.deltaTime, Space.World);
	 }
	 
	  if (mousePos.y >= (Screen.height - VerticalDampner))
	 {
	 	Camera.main.transform.Translate(Vector3.back * MoveSpeed * Time.deltaTime, Space.World);
	 	Camera.main.transform.Translate(Vector3.left * MoveSpeed * Time.deltaTime, Space.World);
	 }
	 
	 if (mousePos.y <= VerticalDampner)
	 {
	 	Camera.main.transform.Translate(Vector3.forward * MoveSpeed * Time.deltaTime, Space.World);
	 	Camera.main.transform.Translate(Vector3.right * MoveSpeed * Time.deltaTime, Space.World);
	 }
}