﻿#pragma strict

var HungerMultiplyer : float = 2;
var MaxMoveSpeed : float = 10;
var WonderingMoveSpeed : float = 2;
private var CurrentMoveSpeed : float = 0;

var WonderingRange : float = 20;
var WonderingTimeRange : float = 3;
private var WonderingTimeCD : float = 0;
private var iTrapped : boolean = false;

var Following : boolean = false;
var Wondering : boolean = false;
private var MyLookAt : GameObject;
var TargetPos : GameObject;
var Scoreboard : Component;
var SplatPrefab : GameObject;
var MyHungerBar : Hunger;

private var MouseOver : boolean = false;

function SelectRandomLocation () : Vector3 {
	return Vector3(Random.Range((transform.position.x - WonderingRange), (transform.position.x + WonderingRange)), 0.5, Random.Range((transform.position.z - WonderingRange), (transform.position.z + WonderingRange)));
}

function MoveTowards(target : GameObject)
{
	if ((Vector3.Distance(target.transform.position, transform.position) > 0.6)) 
	{
		transform.LookAt(target.transform);
		transform.position += transform.forward * CurrentMoveSpeed * Time.deltaTime;
	}
}

function Die () {
	GameObject.Instantiate(SplatPrefab, transform.position, transform.rotation);
	GameObject.Destroy(MyLookAt.gameObject);
	GameObject.Destroy(this.gameObject);
}

function Trap () {
	iTrapped = true;
}

function Move(pos : Vector3) {
	Wondering = false;
	MyLookAt.transform.position = pos;
	Following = true;
}

function Feed () {
	MyHungerBar.ResetHunger();
}

function Start () {	
	MyHungerBar = gameObject.GetComponent.<Hunger>();
	Scoreboard = GameObject.Find("Ground").GetComponent("Score");
	CurrentMoveSpeed = MaxMoveSpeed;
	MyLookAt = new GameObject("SleepLookAt");
	TargetPos = MyLookAt;
	TargetPos.transform.position = transform.position;
	Scoreboard.SendMessage("setDistance", this.gameObject);
}

function Update () {
	MyHungerBar.SetHunger((MyHungerBar.GetHunger() - (Time.deltaTime * HungerMultiplyer)));
	
	if (MyHungerBar.GetHunger() <= 0) {
		Die();
	}
	
	if (iTrapped) {
		Wondering = false;
		Following = false;
	}
	if (Wondering) {
		CurrentMoveSpeed = WonderingMoveSpeed;
		if (WonderingTimeCD <= 0) {
			var choice : int = Random.Range(0, 2);
			//print(choice);
			if (choice == 0) {
				TargetPos.transform.position = transform.position;
				WonderingTimeCD = WonderingTimeRange;
			} else {
				TargetPos.transform.position = SelectRandomLocation();
				//print("Wondering to: " + TargetPos.transform.position);
				WonderingTimeCD = WonderingTimeRange;
			}
		} else {
			WonderingTimeCD -= Time.deltaTime;
			MoveTowards(TargetPos);
		}
	}
	
	if (Following) {
		CurrentMoveSpeed = MaxMoveSpeed;
		MoveTowards(TargetPos);
		if (Vector3.Distance(TargetPos.transform.position, transform.position) <= 0.6) {
			Following = false;
			Wondering = true;
		}
	}
	
	if (MouseOver) {
		if (Input.GetMouseButtonUp(0)) {
			Camera.main.GetComponent("GameInput").SendMessage("Selected", this.gameObject);
		}
	}
}

function OnMouseEnter () {
	transform.GetComponent.<Renderer>().material.color = Color.cyan;
	MouseOver = true;
}

function OnMouseExit () {
	transform.GetComponent.<Renderer>().material.color = Color.white;
	MouseOver = false;
}