﻿#pragma strict
var moveSpeed : float = 10;
var attackDamage : float = 1;
var minDist : float = 3;
var targetRange : float = 6;
var attackCooldown : float = 2.0;

var OpponentTeam : String;
//var StartCommand : GameObject;

private var attackCD = 0.0;
private var startCMD = false;

function Start () {
	attackCD = Time.time;
	//setOpponentTeam("Blue");
	//responseStartCMD(true);
	//StartCommand = GameObject.Find("StateControl");
}

function FixedUpdate () {
	if (startCMD)
	{
		var opponentBase : GameObject;
		var opponentNearest : GameObject;
		
		opponentBase = GameObject.Find(OpponentTeam + "_Base(Clone)");
		
		if (opponentBase != null)
		{
			opponentNearest = FindNearestOpponent();
			if (opponentNearest == null)
			{
				attackMove(opponentBase);
			} else
			{
				var opponentBaseDist = Vector3.Distance(opponentBase.transform.position, transform.position);
				var opponentDist = Vector3.Distance(opponentNearest.transform.position, transform.position);
				
				if (opponentDist < opponentBaseDist) 
				{
					attackMove(opponentNearest);
				} else
				{
					attackMove(opponentBase);
				}
			}
		}
	} else
	{
		requestStartCMD();
	}
}

function FindNearestOpponent () : GameObject {
	var allEnemies : GameObject[];
	allEnemies = GameObject.FindGameObjectsWithTag(OpponentTeam + "_Team");
	var closestEnemy : GameObject;
	var MaxDistance = Mathf.Infinity;
	
	if (allEnemies.Length == 0)
	{
		return null;
	} else
	{
		for (var currEnemy : GameObject in allEnemies)
		{
			var currDistance = Vector3.Distance(currEnemy.transform.position, transform.position);
			if (currDistance < MaxDistance)
			{
				closestEnemy = currEnemy;
				MaxDistance = currDistance;
			}
		}
		return closestEnemy;
	}
}

function moveTowards(target : GameObject)
{
	if ((Vector3.Distance(target.transform.position, transform.position) > minDist)) 
	{
		transform.LookAt(target.transform);
		transform.position += transform.forward * moveSpeed * Time.deltaTime;
	}
}

function attack(target: GameObject)
{
	if (Time.time > attackCD)
	{
		target.GetComponent("Health").SendMessage("Hit", attackDamage);
		attackCD = Time.time + attackCooldown;
	}
}

function attackMove(target: GameObject)
{
	if (Vector3.Distance(target.transform.position, transform.position) < targetRange)
	{
		attack(target);
	} else
	{
		moveTowards(target);
	}
}

function requestStartCMD () {
	//StartCommand.GetComponent("StateCTRL").SendMessage("requestStart", this.gameObject);
}

function responseStartCMD (response : boolean) {
	startCMD = response;
}

function setOpponentTeam (team : String){
	OpponentTeam = team;
	if (team == "Red") {
		transform.GetComponent.<Renderer>().material.color = Color.blue;
		transform.tag = "Blue_Team";
		this.gameObject.layer = 8;	
	} else {
		transform.GetComponent.<Renderer>().material.color = Color.red;
		transform.tag = "Red_Team";
		this.gameObject.layer = 9;	
	}
	
	responseStartCMD(true);
}