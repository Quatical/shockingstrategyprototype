﻿#pragma strict

var OpponentTeam : String;

function Start () {
	transform.name = "Wall";
}

function Update () {

}

function setOpponentTeam (team : String){
	OpponentTeam = team;
	if (team == "Red") {
		transform.GetComponent.<Renderer>().material.color = Color.blue;
		transform.tag = "Blue_Team";
		this.gameObject.layer = 8;	
	} else {
		transform.GetComponent.<Renderer>().material.color = Color.red;
		transform.tag = "Red_Team";
		this.gameObject.layer = 9;	
	}
}