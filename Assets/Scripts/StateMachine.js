﻿#pragma strict
// Blue Team = true & Red Team = false
// Controls the current state progression
var stage : int = 1;
var GUIturn : boolean = true;

// Timer controls
var daytimeCountdown : float = 10.0f;
private var countdown : float = 0.0f;

// Resource handlers
var BuildingGuide : GameObject;
var BlueResource : GameObject;
var  RedResource : GameObject;
var dayToggle : Component;
var theLight : GameObject;

// Loop Breaker
private var iLoop : boolean = false;

var style : GUIStyle;

function Nighttime(Team : boolean) {
	if (iLoop) {
		// Looping
		if (countdown <= 0) {
			BlueResource.GetComponent("Resources").SendMessage("setDay", false);
			RedResource.GetComponent("Resources").SendMessage("setDay", false);
			
			GUIturn = true;
			stage += 1;
			
			if (stage == 5) { 
				stage = 1;
			}
			
			dayToggle.SendMessage("SetTeam", true);
			dayToggle.SendMessage("removeSheep");
			
			dayToggle.SendMessage("SetTeam", false);
			dayToggle.SendMessage("removeSheep");
			
			GetComponent.<Camera>().main.cullingMask = ~(0); // All
			GetComponent.<Camera>().main.cullingMask = ~(1 << 13 | 1 << 12); // No Dragons
			
			iLoop = false;
			
			
		} else {
			countdown -= Time.deltaTime;
		}
	} else {
		// Not looping
		if (Team) {
			dayToggle.SendMessage("SetTeam", true);
			dayToggle.SendMessage("FlipTime", false);
		} else {
			dayToggle.SendMessage("SetTeam", false);
			dayToggle.SendMessage("FlipTime", false);
		}
		
		countdown = daytimeCountdown;
		iLoop = true;
	}
}

function done () {
	countdown = -0.1;
}

function Start () {
	BuildingGuide.SetActive(false);
}

function Update () {
	if (stage == 3) {
		if (GUIturn) {
			
		} else {
			Nighttime(true);
		}
	}
	if (stage == 4) {
		if (GUIturn) {
			
		} else {
			Nighttime(false);
		}
	}
}

function OnGUI () {
	GUI.Label(Rect(((Screen.width / 2) - 295), (Screen.height - 85), 100, 25), "Press Spacebar to place/pick-up", style);
	GUI.Label(Rect (((Screen.width / 2) - 310), (Screen.height - 45), 200, 20), ("Navigate: Move mouse to screen edge"), style);
	if (stage == 1) {
		if (GUIturn) {
			if (GUI.Button(Rect(((Screen.width / 2) - 50), (50), 150, 50), "Blue Day Turn")) {
				Camera.main.transform.position = Vector3(-986, 20, 13);
				GUIturn = false;
				theLight.transform.localRotation.x = 0;
				
				// Start DayTime
				BuildingGuide.SetActive(true);
				BlueResource.GetComponent("Resources").SendMessage("setDay", true);
				dayToggle.SendMessage("SetTeam", true);
				dayToggle.SendMessage("FlipTime", true);
				GetComponent.<Camera>().main.cullingMask = ~(0); // All
				GetComponent.<Camera>().main.cullingMask = ~(1 << 13); // Not Red
			}
		} else {
			//GUI.Label(Rect((Screen.width - 150), (Screen.height - 50), 100, 25), ("Time Left: " + countdown), style);
			if (GUI.Button(Rect(((Screen.width / 2) - 50), (50), 150, 50), "Done")) {
				
				// End DayTime.
				BlueResource.GetComponent("Resources").SendMessage("setDay", false);
				BuildingGuide.SetActive(false);
			
				GUIturn = true;
				stage += 1;
				
				GetComponent.<Camera>().main.cullingMask = ~(0); // All
				GetComponent.<Camera>().main.cullingMask = ~(1 << 13 | 1 << 12); // No Dragons
			}
		}
	}
	
	if (stage == 2) {
		if (GUIturn) {
			if (GUI.Button(Rect(((Screen.width / 2) - 50), (50), 150, 50), "Red Day Turn")) {
				Camera.main.transform.position = Vector3(1013, 20, 13);
				GUIturn = false;
				
				// Start DayTime
				BuildingGuide.SetActive(true);
				RedResource.GetComponent("Resources").SendMessage("setDay", true);
				dayToggle.SendMessage("SetTeam", false);
				dayToggle.SendMessage("FlipTime", true);
				GetComponent.<Camera>().main.cullingMask = ~(0); // All
				GetComponent.<Camera>().main.cullingMask = ~(1 << 12); // Not Blue
			}
		} else {
			//GUI.Label(Rect((Screen.width - 150), (Screen.height - 50), 100, 25), ("Time Left: " + countdown), style);
			if (GUI.Button(Rect(((Screen.width / 2) - 50), (50), 150, 50), "Done")) {
				
				// End DayTime.
				RedResource.GetComponent("Resources").SendMessage("setDay", false);
				BuildingGuide.SetActive(false);
			
				GUIturn = true;
				stage += 1;
				
				GetComponent.<Camera>().main.cullingMask = ~(0); // All
				GetComponent.<Camera>().main.cullingMask = ~(1 << 13 | 1 << 12); // No Dragons
			}
		}
	}
	
	if (stage == 3) {
		if (GUIturn) {
			if (GUI.Button(Rect(((Screen.width / 2) - 50), (50), 150, 50), "Blue Night Turn")) {
				theLight.transform.localRotation.x = 170;
				Camera.main.transform.position = Vector3(1013, 20, 13);
				GUIturn = false;
				Camera.main.transform.position = Vector3(1013, 20, 13);
				
				GetComponent.<Camera>().main.cullingMask = ~(0); // All
				GetComponent.<Camera>().main.cullingMask = ~(1 << 13); // Not Red
			}
		} else {
			GUI.Label(Rect(((Screen.width / 2) - 260), (Screen.height - 130), 100, 25), "Left click to activate follow", style);
			GUI.Label(Rect((Screen.width - 450), (Screen.height - 50), 100, 25), ("Time Left: " + countdown), style);
			if (GUI.Button(Rect(((Screen.width / 2) - 50), (50), 150, 50), "Done")) {
				countdown = -0.1;
			}
		}
	}
	
	if (stage == 4) {
		if (GUIturn) {
			if (GUI.Button(Rect(((Screen.width / 2) - 50), (50), 150, 50), "Red Night Turn")) {
				Camera.main.transform.position = Vector3(-986, 20, 13);
				GUIturn = false;
				Camera.main.transform.position = Vector3(-986, 20, 13);
				
				GetComponent.<Camera>().main.cullingMask = ~(0); // All
				GetComponent.<Camera>().main.cullingMask = ~(1 << 12); // Not Blue
			}
		} else {
			GUI.Label(Rect(((Screen.width / 2) - 260), (Screen.height - 130), 100, 25), "Left click to activate follow", style);
			GUI.Label(Rect((Screen.width - 450), (Screen.height - 50), 100, 50), ("Time Left: " + countdown), style);
			if (GUI.Button(Rect(((Screen.width / 2) - 50), (50), 150, 50), "Done")) {
				countdown = -0.1;
			}
		}
	}
}