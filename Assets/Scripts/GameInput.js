﻿#pragma strict

var PlacementText : GameObject;
var SheepPrefab : GameObject;
var Sheep : GameObject = null;

var placeSheep : boolean = false;
var moveSheep : boolean = true;
var addSheepDelay : float = 0;

var TouchSensitivity : float = 1;
private var TouchStartPos : Vector2;
private var TouchEndPos : Vector2;
private var ZoomStartDistance : float = 0;

function PositionRaycast() {
	var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
	var hit : RaycastHit;
	if (!placeSheep) {
		var newPos : Vector3 = Sheep.transform.position;
	}
	
	if (Physics.Raycast (ray, hit, (Camera.main.transform.position.y + 100))) {
		newPos = hit.point;
		newPos.y = 0.5;
	}
	return newPos;
}

function Selected (sheep : GameObject) {
	Sheep = sheep;
}

function MoveSheep () {
	Sheep.GetComponent("Sheep").SendMessage("Move", PositionRaycast());
	Sheep = null;
}

function AddSheepToggle () {
	addSheepDelay = 0.1;
	MoveSheepLock();
}

function AddSheepLock () {
	placeSheep = !placeSheep;
}

function MoveSheepLock () {
	moveSheep = !moveSheep;
}

function Start () {

}

function Update () {
	if (Input.touchCount == 1) {
		var touch = Input.GetTouch(0);
		
		switch(touch.phase) {
			case TouchPhase.Began:
				TouchStartPos = touch.position;
				break;
			case TouchPhase.Moved:
				TouchEndPos = touch.position;
				var posChange : Vector2 = TouchEndPos - TouchStartPos;
				//print(posChange);
				Camera.main.transform.Translate(Vector3(-posChange.x * TouchSensitivity, -posChange.y * TouchSensitivity, 0));
				TouchStartPos = TouchEndPos;
				break;
			case TouchPhase.Ended:
				break;
		}
	}
	
	if (Input.touchCount == 2) {
		var touch1 : Touch = Input.GetTouch(0);
		var touch2 : Touch = Input.GetTouch(1);
		
		switch(touch1.phase) {
			case TouchPhase.Began:
				ZoomStartDistance = Vector2.Distance(touch1.position, touch2.position);
				break;
			case TouchPhase.Moved:
				var newDistance : float = Vector2.Distance(touch1.position, touch2.position);
				if (newDistance > 1000 || newDistance < 100) {
					print(newDistance);
				}
				Camera.main.transform.Translate(Vector3(0, 0, (newDistance  - ZoomStartDistance) * TouchSensitivity));
				if (Camera.main.transform.position.y < 10) {
					Camera.main.transform.position.y = 10;
				}
				
				if (Camera.main.transform.position.y > 54.7) {
					Camera.main.transform.position.y = 54.7;
				}
				ZoomStartDistance = newDistance;
				break;
			case TouchPhase.Ended:
				break;
		}
	}

	if (moveSheep) {
		if (Sheep != null) {	
			if (Input.GetMouseButtonDown(0)) {
				MoveSheep();
			}
		}
	}	else {
		addSheepDelay -= Time.deltaTime;
		
		if (addSheepDelay <= 0) {
			AddSheepLock();
			PlacementText.SetActive(true);
		}
	}
	
	if (placeSheep) {
		if (Input.GetMouseButtonUp(0)) {
			GameObject.Instantiate(SheepPrefab, PositionRaycast(), Quaternion(0,0,0,0));
			AddSheepLock();
			MoveSheepLock();
			PlacementText.SetActive(false);
		}
	}
}