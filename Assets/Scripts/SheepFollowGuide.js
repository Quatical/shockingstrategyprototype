﻿#pragma strict

function PositionRaycast() {
	var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
	var hit : RaycastHit;
	var newPos : Vector3;
	
	if (Physics.Raycast (ray, hit, 100)) {
		newPos = hit.point;
		newPos.y = 0.25;
		transform.position = newPos;
	}
}

function Start () {
	gameObject.name = "SheepFollowGuide";
}

function Update () {
	PositionRaycast();
}