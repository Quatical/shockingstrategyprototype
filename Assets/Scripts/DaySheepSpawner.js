﻿#pragma strict

var Team : boolean;

var DaySheep : GameObject;

function SetTeam(team : boolean) {
	Team = team;
}

function AddSheep() {
	print("Adding Sheep");
	var spawnPos : Vector3;
	if (Team) {
		spawnPos = Vector3(Random.Range(-1015, -985), 1, Random.Range(-15, 15));
	} else {
		spawnPos = Vector3(Random.Range(985, 1015), 1, Random.Range(-15, 15));
	}
	
	var newSheep : GameObject;
	newSheep = GameObject.Instantiate(DaySheep, spawnPos, transform.rotation);
	newSheep.GetComponent("DaySheep").SendMessage("SetTeam", Team);
}

function RemoveSheep() {
	if (Team) {
		GameObject.Find("DaySheepBlue").GetComponent("DaySheep").SendMessage("Die");
		print("killed");
	} else {
		GameObject.Find("DaySheepRed").GetComponent("DaySheep").SendMessage("Die");
	}
}

function Start () {
	SetTeam(true);
	AddSheep();
	AddSheep();
	AddSheep();
	AddSheep();
	AddSheep();
	SetTeam(false);
	AddSheep();
	AddSheep();
	AddSheep();
	AddSheep();
	AddSheep();
}

function Update () {

}